
//===Create a TO DO list app=========================================================//
"use strict";
const toDoList = () =>{
    const inputData = document.querySelector(".item_input");
    const buttonSubmit = document.querySelector(".btn_submit");
    const buttonSClear = document.querySelector(".btn_clear");
    const newNoteOfList =  document.querySelector(".notes");


    buttonSubmit.addEventListener("click", () => {
        let listItem = document.createElement("li");
        listItem.innerHTML = inputData.value;
        newNoteOfList.append( listItem);

         listItem.addEventListener("click", () => {
             listItem.remove();
         });
        inputData.value = '';
      });

    buttonSClear.addEventListener("click", () => {
        newNoteOfList.innerHTML = '';
    });

};

toDoList();


//====  Memory game  ======================================================================/

const memoryGame = () =>{
    const cards = document.querySelectorAll(".cards__item");
    let firstValue = '';


        cards.forEach((card) => {

          card.addEventListener("click", function () {
            this.classList.toggle("flip");
            let item = this;
            setTimeout(function () {
                if (firstValue == ''){
                    firstValue = item.getAttribute('data-numb');
                } else {
                    if(firstValue == item.getAttribute('data-numb')){
                        alert('Congratulations!');
                    } else{
                        alert('Try again!');
                        cards.forEach((card) => {
                            card.classList.remove("flip");
                        });
                    }
                    firstValue = '';
                }
            }, 500);
        });

    });

};

memoryGame();